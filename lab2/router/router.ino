#include <SoftwareSerial.h>

// Define hardware connections
#define PIN_GATE_IN 4
#define PIN_ANALOG_IN A0

SoftwareSerial xbee(2, 3); // RX, TX

int value ; //Value from pot
String msg;

void setup() {

  //Start the serial communication
  Serial.begin(9600); //Baud rate must be the same as is on xBee module
  xbee.begin(9600);

  
  // configure input to interrupt
  pinMode(PIN_GATE_IN, INPUT);
}

void loop() {

    //Read the analog value from pot and store it to "value" variable
    value = analogRead(PIN_ANALOG_IN);
    int dB = 20 * log10(value);
    
    if (xbee.available()) {
        Serial.println("detected data");
        msg = xbee.readStringUntil('\n');
        msg.remove(msg.length()-1);
        Serial.print(msg.length());
        Serial.print(msg);
        xbee.print(msg);
    }
    
    xbee.print('<');  //Starting symbol
    xbee.print(dB);//Value from 0 to 255
    xbee.println('>');//Ending symbol
    Serial.print('<');  //Starting symbol
    Serial.print(dB);//Value from 0 to 255
    Serial.println('>');//Ending symbol

    delay(100);

}
