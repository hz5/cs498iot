#include <mcp_can.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

#define CAN0_INT 2

long unsigned int rxId;
unsigned char len = 0;
unsigned char distance[2];

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress remote_ip(192, 168, 0, 100);
int remote_port = 3491;
int local_port = 3495;

char packetBuffer[UDP_TX_PACKET_MAX_SIZE];

MCP_CAN CAN0(9);
EthernetUDP udp;

void setup()
{
  Serial.begin(115200);

  // Initialize MCP2515 running at 16MHz with a baudrate of 500kb/s and the masks and filters disabled.
  if(CAN0.begin(MCP_ANY, CAN_500KBPS, MCP_16MHZ) == CAN_OK)
    Serial.println("MCP2515 Initialized Successfully!");
  else
    Serial.println("Error Initializing MCP2515...");

  CAN0.setMode(MCP_NORMAL);                     // Set operation mode to normal so the MCP2515 sends acks to received data.

  pinMode(CAN0_INT, INPUT);                            // Configuring pin for /INT input

  // set up Ethernet
  Ethernet.begin(mac);

  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }

  udp.begin(local_port);
}

void loop()
{
  // receive can signal from ECU
  if(!digitalRead(CAN0_INT))
  {
    CAN0.readMsgBuf(&rxId, &len, distance);
    int result = distance[0]+distance[1]*256;
    Serial.println(result);
  }

  // send the message via udp
  udp.beginPacket(remote_ip, remote_port);
  udp.write((char *)distance);
  udp.endPacket();

  // wait for reply from raspberry
  int packetSize = udp.parsePacket();

  if (packetSize) {
    Serial.print("Received packet from ");
    IPAddress remote = udp.remoteIP();
    for (int i=0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(udp.remotePort());

    // read the packet into packetBufffer
    udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer);
  }
   //send signal to LED
  byte sndStat = CAN0.sendMsgBuf(0x100, 0, 1, (byte*)&packetBuffer);

  delay(500);
}
